#labDebug for MPCS 51031

##FEATURES:


1. This demonstrates the use of the debugger and breakpoints.
2. We can extract methods.
3. We can create file templates.


This may throw an exception.
```
    @Override
    protected void onSaveInstanceState(Bundle outState) {


        outState.putInt("config_change", ++mConfigChangeNum);
        super.onSaveInstanceState(outState);
        Log.d(isNull(outState) + ":" + new Object() {
        }.getClass().getEnclosingMethod().getName().toString(), TAG);
    }
```

####Know bugs
```
04-13 10:30:01.016 32159-32202/edu.uchicago.gerber.labdebug E/EGL_emulation: tid 32202: eglSurfaceAttrib(1174): error 0x3009 (EGL_BAD_MATCH)
04-13 10:30:01.016 32159-32202/edu.uchicago.gerber.labdebug W/OpenGLRenderer: Failed to set EGL_SWAP_BEHAVIOR on surface 0x7876bb0a4f80, error=EGL_BAD_MATCH
04-13 10:30:03.702 32159-32202/edu.uchicago.gerber.labdebug E/EGL_emulation: tid 32202: eglSurfaceAttrib(1174): error 0x3009 (EGL_BAD_MATCH)
04-13 10:30:03.702 32159-32202/edu.uchicago.gerber.labdebug W/OpenGLRenderer: Failed to set EGL_SWAP_BEHAVIOR on surface 0x7876bb0a4f80, error=EGL_BAD_MATCH
04-13 10:30:06.730 32159-32202/edu.uchicago.gerber.labdebug E/EGL_emulation: tid 32202: eglSurfaceAttrib(1174): error 0x3009 (EGL_BAD_MATCH)
04-13 10:30:06.730 32159-32202/edu.uchicago.gerber.labdebug W/OpenGLRenderer: Failed to set EGL_SWAP_BEHAVIOR on surface 0x7876bb0a4f80, error=EGL_BAD_MATCH
04-13 10:30:08.717 32159-32202/edu.uchicago.gerber.labdebug E/EGL_emulation: tid 32202: eglSurfaceAttrib(1174): error 0x3009 (EGL_BAD_MATCH)
04-13 10:30:08.717 32159-32202/edu.uchicago.gerber.labdebug W/OpenGLRenderer: Failed to set EGL_SWAP_BEHAVIOR on surface 0x7876bb0a4f80, error=EGL_BAD_MATCH
04-13 10:30:18.045 32159-32202/edu.uchicago.gerber.labdebug E/EGL_emulation: tid 32202: eglSurfaceAttrib(1174): error 0x3009 (EGL_BAD_MATCH)
04-13 10:30:18.045 32159-32202/edu.uchicago.gerber.labdebug W/OpenGLRenderer: Failed to set EGL_SWAP_BEHAVIOR on surface 0x7876bb0a4f80, error=EGL_BAD_MATCH
04-13 10:49:48.430 32159-32166/edu.uchicago.gerber.labdebug W/art: Suspending all threads took: 6.628ms

```

Markdown style guide:
https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html


![My Darcula theme] (https://s3.amazonaws.com/infinum.web.production/repository_items/files/000/000/167/original/android-studio-2.png)
![launcher icon] (/app/src/main/res/mipmap-hdpi/ic_launcher.png)

![launcher icon] (/app/src/main/res/mipmap-hdpi/ic_launcher.png)

