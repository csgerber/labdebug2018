package edu.uchicago.gerber.labdebug;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    //////////////////////////////////////////////////////
    //                 Step one: define fields
    //////////////////////////////////////////////////////

    private TextView textHello;
    private EditText editName;
    private Button buttonStart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //////////////////////////////////////////////////////
        //                 Step 2: inflate and get references
        //////////////////////////////////////////////////////

        setContentView(R.layout.activity_main);

        textHello = (TextView) findViewById(R.id.textHello);
        editName = (EditText) findViewById(R.id.editName);
        buttonStart = (Button) findViewById(R.id.buttonStart);

        //////////////////////////////////////////////////////
        //                 Step 3: define behavior
        //////////////////////////////////////////////////////

        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAge();
            }
        });


        editName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
               if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:

                            showAge();

                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });


    }

    private void showAge() {
        int nAge = 0;
        try {
            nAge = Integer.parseInt(editName.getText().toString());
        } catch (NumberFormatException e) {

            editName.setText("");
            editName.requestFocus();
            Toast.makeText(MainActivity.this, "You need to enter an integer", Toast.LENGTH_SHORT).show();
            return;
        }

        Toast.makeText(MainActivity.this, "Your age is " + nAge, Toast.LENGTH_LONG).show();
    }
}
